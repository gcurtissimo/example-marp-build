---
marp: true
theme: example
paginate: true
footer: Slide deck 1
---

<!-- All classes are defined in the marp-theme.css -->

<!-- _class: title -->

# The title for you

## A subtitle

---

<!-- _class: odd single-image -->

![height:650px](./images/get-to-work.jpg)

<!--

These are speaker notes.

They are not parsed as HTML, nor Markdown, so just type out what you want.

-->

---

<!-- _class: even two-column -->

# You should work

Because that's the way it is

<div class="right">

1. Get a job
1. Work really hard
1. Die

</div>
<div class="left self-centered">

![height:400px](./images/quality.jpg)

</div>

<!--

More speaker notes, you know?

--->

---

<!-- _class: odd -->

# Work shifts

|  Start   |   End    | Job                    |
|:--------:|:--------:|------------------------|
| midnight |   noon   | Work on all the things |
|   noon   | midnight | Just keep working...   |

---

<!-- _class: even single-image -->

# Workflow

![do it](./images/workflow.dot.svg)

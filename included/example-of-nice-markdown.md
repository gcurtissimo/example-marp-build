# Included file

This is a really long line that has been wrapped to comply
with the linting rules for Markdown.

## Here's a correct second heading before the H3

### Down to the third

There are no misspelled words in this sentence.

This would [pass](https://curtis.schlak.com) because the
link exists.

| Column 1                   | Column 2                       |
|----------------------------|--------------------------------|
| This **will** pass linting | because                        |
| this table                 | is nicely formatted based      |
| on what                    | markdown-table-formatter wants |

```python
def it_has_a_language_specifier():
    print("So, everything is okay")
```

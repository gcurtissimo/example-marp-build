# Example Learn CI/CD Build

This is an example repository that shows linting, spell
checking, and presentation building and publishing for
Learn-like content.

<!-- markdownlint-disable MD013 -->
```mermaid
flowchart LR
  start["<b>LINT</b>\nCheck files for consistent\nMarkdown source and\nspelling errors"]
  --> html["<b>PRESENTATIONS</b>\nGenerate HTML files from\nMarkdown files and Graphviz\ndiagrams from <code>dot</code> files"]
  --> publish["<b>PUBLISH</b>\nGenerate a table of contents\nand publish the HTML to\nGitLab pages"]
```
<!-- markdownlint-enable MD013 -->

You can see the published [Presentation Table of
Contents](https://gcurtissimo.gitlab.io/example-marp-build/)
by clicking on the link.

Instructors can:

* Use the HTML slides to present using the Speaker View
* Distribute the PDF slides to students for their reference

## Slide theme

Open the [marp-theme.css](./included/marp-theme.css) file to
update and change the styling used to generate the HTML
versions of the files.

## Writing presentations in Visual Studio Code

Install the [Marp for VS
Code](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode)
extension. Then, you can see Marp files with the command
_Markdown: Open Preview to the Side_.

## Run linting manually

For *NIX shells:

```sh
docker run \
  -e "REPORT_OUTPUT_FOLDER=/tmp/lint/.linter" \
  -v "$(pwd):/tmp/lint" \
  oxsecurity/megalinter-documentation:v6
```

For PowerShell:

```powershell
docker run `
  -e "REPORT_OUTPUT_FOLDER=/tmp/lint/.linter" `
  -v "$(pwd):/tmp/lint" `
  oxsecurity/megalinter-documentation:v6
```

## Set up pre-commit hooks

If you want to run this automatically for each commit, then
we recommend that you install the `pre-commit` framework. It
will use the
[.pre-commit-config.yaml](./.pre-commit-config.yaml)
file to run the image listed above.

1. Install the [pre-commit](https://pre-commit.com/)
   framework
1. Install the hook:

    ```sh
    pre-commit install
    ```

1. You can run the linter manually, now, with this command:

    ```sh
    pre-commit run --all-files
    ```

## Building Graphviz diagrams locally

You can render the `*.dot` files to SVG using the following
command after you install Graphviz for your system.

<!-- spellchecker: disable -->
<!-- markdownlint-disable MD013 -->
```sh
find included \
  -name '*.dot' \
  -type f \
  -exec sh -c "dot -Tsvg {} | tail -n+6 | sed -E 's/svg[[:space:]]+width=\"[^\"]+\"/svg/g' | sed -E 's/svg[[:space:]]+height=\"[^\"]+\"/svg/g' > {}.svg" \;
```
<!-- spellchecker: enable -->
<!-- markdownlint-enable MD013 -->

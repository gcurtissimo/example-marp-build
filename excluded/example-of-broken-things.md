## Breaks because the first header should be a `#`

# Not included

# This would break because of two H1s

### This would break because it skips a `##`
### This breaks because there should be a blank line around headers

This is a really long line that should not pass the linting for Markdown. But, because it's excluded, it's okay.

Did I mention the mispelled word, too?





There are too many blank lines which would break the linter.

This would also [fail](http://localhost:1) because the
link does not exist.

| Column 1 | Column 2 |
|-|-|
| This will not passed | because |
| this table | is not well formatted based |
| on what | markdown-table-formatter will determine |


```
breaks because of no language specified
```
